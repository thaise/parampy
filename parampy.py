
# coding: utf-8

# In[1]:


#### Functions to be used with PARAM files

import numpy
import pandas
import matplotlib
import os.path
import math
import glob
import matplotlib.gridspec

# Global filenames: 
pdf_files = ["pdfs/PDFage/pdf_age_", "pdfs/PDFm/pdf_m_", "pdfs/PDFr/pdf_r_", "pdfs/PDFlogg/pdf_logg_",
             "pdfs/PDFmu0/pdf_mu0_", "pdfs/PDFdist/pdf_d_", "pdfs/PDFpar/pdf_par_", "pdfs/PDFav/pdf_av_"]
pdf_file_lum = ["pdfs/PDFlogLum/pdf_logLum_"]
pdf_file_logTeff = ["pdfs/PDFlogte/pdf_logte_"]
pdf_file_dnu = ["pdfs/PDFDnu/pdf_dnu_"]
pdf_file_mh = ["pdfs/PDFmh/pdf_mh_"]
pdf_mag_files = ["pdfs/PDFmagobs/pdf_magobs_", "pdfs/PDFmagobs0/pdf_magobs0_", "pdfs/PDFmag/pdf_mag_"]

# Global parameters:
parameters = ["age", "mass", "rad", "logg", "mu0", "dist", "par", "Av"]


# In[2]:


# To be used with Python 2 and 3
try:
  basestring
except NameError:
  basestring = str


# In[3]:


class read_param_inp_outs():
    '''
        A class that keeps information about PARAM directory
        and read and store the PARAM input and output files
        in one pandas dataframe.
        
        read_param_inp_outs(self, pdfs_dir, *args)
        
        Parameters:
            pdfs_dir: string - PARAM main directory
        
        Args:
            median: to read median output
            inp_mo_pdf: to read input mode output
            inp_me_pdf: to read input median output
        
        Returns: 
            a pandas dataframe with all 
            concatenated files by identifier
    '''
    def __init__(self, pdfs_dir, *args):
        self.dir = pdfs_dir
        # Reading median?
        read_median = 0
        read_inp_mo = 0
        read_inp_me = 0

        # Reading PARAM files:
        df_inp =  read_par_file(pdfs_dir+"/*param.in")
        df_moout = read_par_file(pdfs_dir+"/*param_mo.out")
        df_moout = df_moout.add_suffix('_mo')
        for arg in args:
            if arg == 'median':
                read_median = 1
                df_meout = read_par_file(pdfs_dir+"/*param_me.out")
                df_meout = df_meout.add_suffix('_me')
            if arg == 'inp_mo_pdf':
                read_inp_mo = 1
                df_inpmoout = read_par_file(pdfs_dir+"/*input*_mo.out")
                df_inpmoout = df_inpmoout.add_suffix('_imo')
            if arg == 'inp_me_pdf':
                read_inp_me = 1 
                df_inpmeout = read_par_file(pdfs_dir+"/*input*_me.out")
                df_inpmeout = df_inpmeout.add_suffix('_ime') 

        # Merging dataframes
        #df_inp.iloc[:,0] = df_inp.iloc[:,0].astype(int)
        if (read_median == 1):
            df_out = pandas.merge(df_moout, df_meout, how='inner',
                                  left_on=df_moout.columns[0], right_on=df_meout.columns[0],
                                  sort=False)
            self.df = pandas.merge(df_inp, df_out, how='inner',
                              left_on=df_inp.columns[0], right_on=df_out.columns[0], sort=False)
            self.df.drop([df_moout.columns[0], df_meout.columns[0]], axis=1, inplace=True)
            
        else:
            self.df = pandas.merge(df_inp, df_moout, how='inner',
                              left_on=df_inp.columns[0], right_on=df_moout.columns[0], sort=False)
            self.df.drop([df_moout.columns[0]], axis=1, inplace=True)
        
        if (read_inp_mo == 1):   
            self.df = self.df.merge(df_inpmoout, how='inner',
                                    left_on=self.df.columns[0], right_on=df_inpmoout.columns[0], sort=False)
            self.df.drop([df_inpmoout.columns[0]], axis=1, inplace=True)
            
        if (read_inp_me == 1):   
            self.df = self.df.merge(df_inpmeout, how='inner',
                                    left_on=self.df.columns[0], right_on=df_inpmeout.columns[0], sort=False)
            self.df.drop([df_inpmeout.columns[0]], axis=1, inplace=True)   
            
        # Finding NaN values (<-90.0)
        self.df[self.df.select_dtypes(include='number') < -90.0] = pandas.np.nan

def read_par_file(filename):
    '''
        Reads a PARAM file and returns a pandas dataframe.
        
        df = read_par_file(filename)
        
        Paramenter:
            filename: string - PARAM filename
            
        Returns: a pandas dataframe
    '''
    fname = glob.glob(filename)
    if len(fname) != 1:
        print ("ERROR: Check if "+ filename + "  input file exists.")
        return None
    print ("Reading " +  fname[0])
    df = pandas.read_csv(fname[0], sep='\s+')
    df.columns = df.columns.str.replace('#Id', 'Id')
    df.columns = df.columns.str.replace('#', '')
    return df


# In[1]:


def plot_npdfs(star, obj_a, label_a, obj_b, label_b, *args, **kwargs):
    '''
    Plots a figure with pdfs of age, mass, radius, logg, mu0, dist,
    parallax, and extinction for at least two cases
    with the mode as default central tendency.
    
    plot_pdfs(star, obj_a, label_a, obj_b, label_b, *args, **kwargs)
    
    Parameters:
        star: integer - number of the star identification 
        obj_a, obj_b: read_param_inp_outs objects
        label_a, label_b: string - label for case a and case b
    *args:
        obj: new read_param_inp_outs object
        label: label for the new object case 
        median: to plot the median as central tendency (default mode)
    **kwargs:
        name_par_col = string - name of input parallax column
        title = string - figure title
        savefig = string - file path to save the figure
        **keywords: options to pass to matplotlib plotting method
    
    '''
    global pdf_files
    global parameters
    
    param = [s + '_mo' for s in parameters]
    # median?
    for arg in args:
        if arg == 'median':
            param = [s + '_me' for s in parameters]
                
    dfs = [obj_a.df.loc[obj_a.df[obj_a.df.columns[0]]==star],
           obj_b.df.loc[obj_b.df[obj_b.df.columns[0]]==star]]
    pdf_dirs = [obj_a.dir, obj_b.dir]
    labels = [label_a, label_b]
    colors = ('C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7')
    for arg in args:
        if isinstance(arg, read_param_inp_outs):
            dfs = dfs + [arg.df.loc[arg.df[arg.df.columns[0]]==star]]
            pdf_dirs = pdf_dirs + [arg.dir]
        #try:
        if isinstance(arg, basestring):
               labels = labels + [arg]
        #except NameError:
                #basestring = str
                #if isinstance(arg, basestring):
                    #labels = labels + [arg]
            
    xlabel = [u"log (age/yr)", u"log ($M/M_\odot$)", u"log ($R/R_\odot)$", u"log g",
              u"$\mu_0$ (mag)", u"dist (pc)", u"parallax (mas)", u"$A_V$ (mag)"]
    max_y = [[]] * 8
    
    # Checking if the star converged in all dataframes:
    check_ct = []
    for i in range(0,len(dfs)):
        check_ct = check_ct + [dfs[i][param[1]].values]
        
    if (numpy.isnan(check_ct).all()):
        print("This star did not converge. Cannot plot!")  
        return None
    
    # ploting
    f, axis = matplotlib.pyplot.subplots(2, 4, figsize=(16, 8))
    f.subplots_adjust(hspace=0.4, wspace=0.4)

    for i in range(0,len(dfs)):    # loop in pdf_dirs
        for file_, ax, j in zip(pdf_files, axis.flat, range(0,8)): # loop in pdf_files
            if os.path.exists(pdf_dirs[i]+file_+str(star))==False:
                continue
            pdf = pandas.read_csv(pdf_dirs[i]+file_+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            if pdf.empty:
                continue
            delta = pdf.iloc[1,0]-pdf.iloc[0,0]
            for imin in range(0, len(pdf.iloc[:,0]), 1):
                if (pdf.iloc[imin,1]*delta >= 0.00001):
                    break
            for imax in range(len(pdf.iloc[:,0])-1, 0, -1):
                if (pdf.iloc[imax,1]*delta >= 0.00001):
                    break        
            ax.plot(pdf.iloc[imin:imax,0], pdf.iloc[imin:imax,1],
                    color=colors[i], label=labels[i]) #s=10,
            #pdf.plot.line(pdf.columns[0], pdf.columns[1], ax=ax,
            #                 color=colors[i], label=labels[i]) #s=10,
            # To build the legend 
            if (j==1):
                handles, labels_plotted = ax.get_legend_handles_labels() 
            max_y[j] = max_y[j] + [max(pdf[pdf.columns[1]]) + max(pdf[pdf.columns[1]])/4.0]
            ax.set_ylim([0.0,max(max_y[j])])
            ct = float(dfs[i][param[j]]) # central tendency
            if (j==0):
                ct = math.log(dfs[i][param[j]], 10.0) + 9.0
            if (j>0 and j<3):
                ct = math.log(dfs[i][param[j]], 10.0)
            #print central tendency
            if ct is not None:
                ax.plot([ct, ct], [0,5000.0], linestyle='--', color=colors[i])
            # plot input parallax
            pos_par_col = 0
            if 'name_par_col' not in kwargs: 
                if ('Avinp') in obj_a.df.columns:
                    pos_par_col = obj_a.df.columns.get_loc('Avinp')+4
                elif ('av_min') in obj_a.df.columns:
                    pos_par_col = obj_a.df.columns.get_loc('av_min')+4
                else:
                    pos_par_col = 0
            else:
                if kwargs['name_par_col'] in obj_a.df.columns:
                    pos_par_col = obj_a.df.columns.get_loc(kwargs['name_par_col'])
            if (i==0 and j==6 and pos_par_col>0):
                inp_par = float(dfs[i][dfs[i].columns[pos_par_col]])
                ax.plot([inp_par,inp_par], [0,5000.0], linestyle='--', color='black', label="input")
            #ax.legend()
            ax.tick_params(axis='both', direction='in', top=True, right=True)
            ax.set_xlabel(xlabel[j])
            ax.set_ylabel('PDF')
    
    # Legend
    f.legend(handles, labels_plotted, loc=8, ncol=len(dfs), fontsize='large')
    
    fig_file = None
    for key, value in kwargs.items():
        if key == 'title':
            matplotlib.pyplot.suptitle(value, fontsize=16)
        if key == 'savefig':
            fig_file = value
                
    matplotlib.pyplot.show()
    if fig_file is not None:
        f.savefig(fig_file, bbox_inches='tight')
    


# In[8]:


def get_magnitude_names(obj, **kwargs):
    '''
    Get apparent magnitude and error column names used to run PARAM.
    
    names, snames = get_magnitude_names(obj)
    
    Parameters:
        obj: read_param_inp_outs object
        
    **kwargs:
        last_mag_col: string - name of the last magnitude column
    
    Returns:
        two lists with names of apparent magnitude and its errors columns in the object
    '''
    
    lmagcol=16
    names = []
    snames = []
    if ('Avinp') in obj.df.columns:
        lmagcol = obj.df.columns.get_loc('avinp')
    elif ('av_min') in obj.df.columns:
        lmagcol = obj.df.columns.get_loc('av_min')
    if 'last_mag_col' in kwargs:
        if kwargs['last_mag_col'] in obj.df.columns:
            lmagcol = obj.df.columns.get_loc(kwargs['last_mag_col'])+2
        else:
            print("ERROR: there is no "+kwargs['last_mag_col']+" in object.")
            return None
        
    if (lmagcol==16):
        print("ERROR: can't get magnitude names")
        return None
    else:
        for st in range(16, lmagcol, 2):
            names = names + [obj.df.columns[st]]
            snames = snames + [obj.df.columns[st+1]]
        return names, snames


# In[2]:


def plot_npdfs_mag(star, obj_a, label_a, obj_b, label_b, *args, **kwargs):
    '''
    Plots PDFs of apparent and absolute magnitudes
    for at least two cases.
    
    plot_pdfs_mag(star, df_a, df_b, dir_a, dir_b, label_a, label_b,
                  *args, **kwargs)
    
    Parameters:
        star: integer - number of the star identification 
        obj_a, obj_b: read_param_inp_outs objects
        label_a, label_b: string - label for case a and case b
    *args:
        obj: new read_param_inp_outs object
        label: label for the new object case
        median: to take into account the median values
    **kwargs:
        pdf_files = string or list of strings - PDF filenames
        title = string - figure title
        savefig = string - file path to save the figure
        **keywords: options to pass to matplotlib plotting method or
            get_magnitude_names
    '''
    global parameters
    
    param = [s + '_mo' for s in parameters]
    # median?
    for arg in args:
        if arg == 'median':
            param = [s + '_me' for s in parameters]
    
    dfs = [obj_a.df.loc[obj_a.df[obj_a.df.columns[0]]==star],
           obj_b.df.loc[obj_b.df[obj_b.df.columns[0]]==star]]
    pdf_dirs = [obj_a.dir, obj_b.dir]
    labels = [label_a, label_b]
    for arg in args:
        if isinstance(arg, read_param_inp_outs):
            dfs = dfs + [arg.df.loc[arg.df[arg.df.columns[0]]==star]]
            pdf_dirs = pdf_dirs + [arg.dir]
        #try:
        if isinstance(arg, basestring):
               labels = labels + [arg]            

    pdf_files = ("pdfs/PDFmagobs0/pdf_magobs0_", "pdfs/PDFmag/pdf_mag_")
    if 'pdf_files' in kwargs:
        pdf_files = kwargs["pdf_files"]
 
    # labels
    x_labels = ("$m_\lambda$", "$M_\lambda$")
    y_labels = "PDF"
    leg_labels = ['mbol'] + get_magnitude_names(obj_a, **kwargs)[0]

    max_x = [[]] * 2
    min_x = [[]] * 2
    
    # Checking if the star converged in all dataframes:
    check_ct = []
    for i in range(0,len(dfs)):
        check_ct = check_ct + [dfs[i][param[1]].values]
    if (numpy.isnan(check_ct).all()):
        return None
    
    # ploting
    f, axis = matplotlib.pyplot.subplots(len(dfs), 2, figsize=(16, 4*len(dfs)))
    f.subplots_adjust(hspace=0.2, wspace=0.2)

    for i in range(0,len(dfs)): # loop pdf_dirs (rows)      
        for file_, j in zip(pdf_files, range(0,2)): # loop pdf_files (cols)
            if os.path.exists(pdf_dirs[i]+file_+str(star))==False:
                if (j!=0) and os.path.exists(pdf_dirs[i]+"pdfs/PDFmagobs0/pdf_magobs0_"+str(star))==False:
                    continue
                else:
                    file_ = "pdfs/PDFmagobs/pdf_magobs_"
                    if os.path.exists(pdf_dirs[i]+file_+str(star))==False:
                        continue
            pdf = pandas.read_csv(pdf_dirs[i]+file_+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            if pdf.empty:
                continue
            # number of available filters -- only use filters based on the observed magnitudes:
            if (i==0 and j==0):
                filters = numpy.unique(pdf.iloc[:,0])
            for fil in filters:
                pdf[pdf.fil==fil].plot.line(pdf.columns[1], pdf.columns[2], ax=axis[i,j],
                                            #color=matplotlib.cm.rainbow(norm(fil)),
                                            label=leg_labels[fil], legend=None )
                # To build the legend -- only use filters based on the observed magnitudes:
                if (i==0 and j==0):
                    handles, labels_plotted = axis[i,j].get_legend_handles_labels() 
            max_x[j] = max_x[j] + [max(pdf[pdf.columns[1]])]
            min_x[j] = min_x[j] + [min(pdf[pdf.columns[1]])]
            axis[i,j].set_xlabel(x_labels[j])
            axis[i,j].set_ylabel(y_labels +' (' +labels[i]+')')

    for j in range(0,2):
        for i in range(0, len(dfs)):
            axis[i,j].set_xlim([0.95*min(min_x[j]),1.05*max(max_x[j])])

    # Legend
    f.legend(handles, labels_plotted, loc=8, ncol=len(filters), fontsize='large')
    
    if 'title' in kwargs:
        matplotlib.pyplot.suptitle(kwargs['title'], y=0.92, fontsize=16)
                
    matplotlib.pyplot.show()

    if 'savefig' in kwargs:
        f.savefig(kwargs["savefig"], bbox_inches='tight') 


# In[2]:


def plot_pdfs_m_M_mu_mu0(star, obj, *args, **kwargs):
    '''
    Plots PDFs of apparent, absolute magnitudes, apparent distance modulus,
    and distance modulus for one star.
    
    plot_pdfs_m_M_mu_mu0(star, obj, *args, **kwargs)
    
    Parameters:
        star: integer - number of the star identification 
        obj: read_param_inp_outs objects
    *args:
        median: to take into account the median values
    **kwargs:
        pdf_files: list of strings with PDF magobs, mag, mu and mu0 filenames
        fil_names: list of strings with filter names sorted according to
                    PARAM tab_mag file
        alamb_av: list of floats with alamb_av coefficients sorted according to
                    PARAM tab_mag file. The first value must be 0.0, and
                    the len should be (1 + number of filters).
        title: string - figure title
        savefig: string - file path to save the figure
        **keywords: options to pass to matplotlib plotting method or
            get_magnitude_names
    '''
    global parameters
    
    # Default:
    param = [s + '_mo' for s in parameters]
    uparam = [s + '_68U_mo' for s in parameters]
    lparam = [s + '_68L_mo' for s in parameters]
    # median?
    for arg in args:
        if arg == 'median':
            param = [s + '_me' for s in parameters]
            uparam = [s + '_68U_me' for s in parameters]
            lparam = [s + '_68L_me' for s in parameters]

    df = obj.df.loc[obj.df[obj.df.columns[0]]==star]
    pdf_dir = obj.dir
    
    if 'pdf_files' in kwargs:
        pdf_files = kwargs['pdf_files']
    else:
        pdf_files = ["pdfs/PDFmagobs0/pdf_magobs0_", "pdfs/PDFmag/pdf_mag_",
                     "pdfs/PDFmu/pdf_mu_", "pdfs/PDFmu0/pdf_mu0_"]    
    if 'fil_names' in kwargs:
        leg_labels = ['mbol'] + kwargs['fil_names']
    else:
        leg_labels = ['mbol'] + get_magnitude_names(obj, **kwargs)[0]

    x_labels = ("$m_\lambda$", "$M_\lambda$", "$\mu_\lambda$", "$\mu_{0\lambda}$")
    y_labels = "PDF"
    # Checking if the star converged:
    if (numpy.isnan(df[param[1]])).all():
        return None
    
    # ploting
    f, axis = matplotlib.pyplot.subplots(2, 2, figsize=(16,10))
    f.subplots_adjust(hspace=0.2, wspace=0.2)

    for ax, file, i in zip(axis.flat, pdf_files, range(0,4)):          
        if not os.path.exists(pdf_dir+file+str(star)):
            if (i!=0) and os.path.exists(pdf_dir+"pdfs/PDFmagobs0/pdf_magobs0_"+str(star))==False:
                continue
            else:
                file = "pdfs/PDFmagobs/pdf_magobs_"
                if os.path.exists(pdf_dir+file+str(star))==False:
                    continue
        if (i<3):                
            pdf = pandas.read_csv(pdf_dir+file+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            # number of available filters -- only use filters based on the observed magnitudes:
            if (i==0):
                filters = numpy.unique(pdf.iloc[:,0])
            for fil in filters:
                pdf[pdf.fil==fil].plot.line(pdf.columns[1], pdf.columns[2], ax=ax,
                                            label=leg_labels[fil], legend=None )
                # To build the legend -- only use filters based on the observed magnitudes:
                if (i==0):
                    handles, labels_plotted = ax.get_legend_handles_labels() 

            ax.set_xlim([0.98*min(pdf[pdf.columns[1]]),1.02*max(pdf[pdf.columns[1]])])

        else:
            if 'alamb_av'in kwargs:
                alamb_av = kwargs['alamb_av']
            else:                
                if not os.path.isdir(pdf_dir+"/pdfs/AlambAv/"):
                    print("ERROR: Missing Alambda_Av information.")
                    return None
                else:
                    alamb_av = pandas.read_csv(pdf_dir+"/pdfs/AlambAv/alamb_av_"+str(star), sep="\s+")
                    alamb_av.columns = alamb_av.columns.str.replace('#', '')   
    
            for fil in filters:
                # mu0 = mu_lambda - Alamb_Av * Av
                Av = df[param[7]].values
                AvU = df[uparam[7]].values
                AvL= df[lparam[7]].values
                if 'alamb_av'in kwargs:
                    pdf.loc[pdf.fil==fil,'mu0']=pdf.loc[pdf.fil==fil,pdf.columns[1]] -                     alamb_av[fil] * Av
                else:
                    pdf.loc[pdf.fil==fil,'mu0']=pdf.loc[pdf.fil==fil,pdf.columns[1]] -                     alamb_av[alamb_av.columns[fil]].values * Av
                
                pdf[pdf.fil==fil].plot.line('mu0', pdf.columns[2], ax=ax,
                                            label=leg_labels[fil], legend=None)
                
            ax.set_xlim([0.98*min(pdf['mu0']),1.02*max(pdf['mu0'])])
            
            # ploting mu0
            pdf = pandas.read_csv(pdf_dir+file+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            pdf.plot.line(pdf.columns[0], pdf.columns[1], ax=ax,
                          c='black', linewidth=2.5,
                          legend=None )
            av_leg = '$A_V$={:.2f}\n[{:.2f},{:.2f}]'.format(float(Av), float(AvL), float(AvU))
            ax.annotate(av_leg, xy=(0.75, 0.8), xycoords='axes fraction', fontsize=18,
                xytext=(-5, -5), textcoords='offset points',
                ha='left', va='bottom')

        ax.set_xlabel(x_labels[i])
        ax.set_ylabel(y_labels)


    # Legend
    f.legend(handles, labels_plotted, loc=8, ncol=len(filters), fontsize='large')
    
    if 'title' in kwargs:
        matplotlib.pyplot.suptitle(kwargs["title"], y=0.92, fontsize=16)
                
    matplotlib.pyplot.show()

    if 'savefig' in kwargs:
        f.savefig(kwargs["savefig"], bbox_inches='tight') 


# In[ ]:


############### TEST PHASE! DO NOT USE IT ################
def plot_pdfs_m_M_mu_mu0_fitAv(star, obj, Av, *args, **kwargs):
    '''
    TEST PHASE! DO NOT USE IT
    Plots PDFs of apparent, absolute magnitudes, apparent distance modulus,
    and distance modulus for one star.
    
    plot_pdfs_m_M_mu_mu0(star, obj, *args, **kwargs)
    
    Parameters:
        star: integer/string - star identification 
        obj: read_param_inp_outs objects
    *args:
        median: to take into account the median values
    **kwargs:
        pdf_files: list of strings with PDF magobs, mag, mu and mu0 filenames
        fil_names: list of strings with filter names sorted according to
                    PARAM tab_mag file
        alamb_av: list of floats with alamb_av coefficients sorted according to
                    PARAM tab_mag file
        title: string - figure title
        savefig: string - file path to save the figure
        **keywords: options to pass to matplotlib plotting method or
            get_magnitude_names
    '''
    global parameters
    
    # Default:
    param = [s + '_mo' for s in parameters]
    uparam = [s + '_68U_mo' for s in parameters]
    lparam = [s + '_68L_mo' for s in parameters]
    # median?
    for arg in args:
        if arg == 'median':
            param = [s + '_me' for s in parameters]
            uparam = [s + '_68U_me' for s in parameters]
            lparam = [s + '_68L_me' for s in parameters]
                      
    df = obj.df.loc[obj.df[obj.df.columns[0]]==star]
    pdf_dir = obj.dir
    
    if 'pdf_files' in kwargs:
        pdf_files = kwargs['pdf_files']
    else:
        pdf_files = ["pdfs/PDFmagobs0/pdf_magobs0_", "pdfs/PDFmag/pdf_mag_",
                     "pdfs/PDFmu/pdf_mu_", "pdfs/PDFmu0/pdf_mu0_"]    
    if 'fil_names' in kwargs:
        leg_labels = ['mbol'] + kwargs['fil_names']
    else:
        leg_labels = ['mbol'] + get_magnitude_names(obj, **kwargs)[0]

    x_labels = ("$m_\lambda$", "$M_\lambda$", "$\mu_\lambda$", "$\mu_{0\lambda}$")
    y_labels = "PDF"
    # Checking if the star converged:
    if (numpy.isnan(df[param[1]])).all():
        return None
    
    # ploting
    f, axis = matplotlib.pyplot.subplots(2, 2, figsize=(16,10))
    f.subplots_adjust(hspace=0.2, wspace=0.2)

    for ax, file, i in zip(axis.flat, pdf_files, range(0,4)):          
        if not os.path.exists(pdf_dir+file+str(star)):
            if (i!=0) and os.path.exists(pdf_dir+"pdfs/PDFmagobs0/pdf_magobs0_"+str(star))==False:
                continue
            else:
                file = "pdfs/PDFmagobs/pdf_magobs_"
                if os.path.exists(pdf_dir+file+str(star))==False:
                    continue
        if (i<3):                
            pdf = pandas.read_csv(pdf_dir+file+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            # number of available filters -- only use filters based on the observed magnitudes:
            if (i==0):
                filters = numpy.unique(pdf.iloc[:,0])
            for fil in filters:
                pdf[pdf.fil==fil].plot.line(pdf.columns[1], pdf.columns[2], ax=ax,
                                            label=leg_labels[fil], legend=None )
                # To build the legend -- only use filters based on the observed magnitudes:
                if (i==0):
                    handles, labels_plotted = ax.get_legend_handles_labels() 

            ax.set_xlim([0.98*min(pdf[pdf.columns[1]]),1.02*max(pdf[pdf.columns[1]])])

        else:
            if 'alamb_av'in kwargs:
                alamb_av = kwargs['alamb_av']
            else:                
                if not os.path.isdir(pdf_dir+"/pdfs/AlambAv/"):
                    print("ERROR: Missing Alambda_Av information.")
                    return None
                else:
                    alamb_av = pandas.read_csv(pdf_dir+"/pdfs/AlambAv/alamb_av_"+str(star), sep="\s+")
                    alamb_av.columns = alamb_av.columns.str.replace('#', '')   
    
            for fil in filters:
                # mu0 = mu_lambda - Alamb_Av * Av
                pdf.loc[pdf.fil==fil,'mu0']=pdf.loc[pdf.fil==fil,pdf.columns[1]] -                 alamb_av[alamb_av.columns[fil]].values * Av
                
                pdf[pdf.fil==fil].plot.line('mu0', pdf.columns[2], ax=ax,
                                            label=leg_labels[fil], legend=None)
                
            ax.set_xlim([0.98*min(pdf['mu0']),1.02*max(pdf['mu0'])])
            
            # ploting mu0
            pdf = pandas.read_csv(pdf_dir+file+str(star), sep=' ')
            pdf.columns = pdf.columns.str.replace('#', '')
            pdf.plot.line(pdf.columns[0], pdf.columns[1], ax=ax,
                          c='black', linewidth=2.5,
                          legend=None )
            av_leg = '$A_V$={:.2f}'.format(float(Av))
            ax.annotate(av_leg, xy=(0.75, 0.8), xycoords='axes fraction', fontsize=18,
                xytext=(-5, -5), textcoords='offset points',
                ha='left', va='bottom')

        ax.set_xlabel(x_labels[i])
        ax.set_ylabel(y_labels)


    # Legend
    f.legend(handles, labels_plotted, loc=8, ncol=len(filters), fontsize='large')
    
    if 'title' in kwargs:
        matplotlib.pyplot.suptitle(kwargs["title"], y=0.92, fontsize=16)
                
    matplotlib.pyplot.show()

    if 'savefig' in kwargs:
        f.savefig(kwargs["savefig"], bbox_inches='tight') 


# In[2]:


class read_pdfs():
    '''
        A class that keeps info about a single star
        and reads and stores the PDF files in dataframes.
        
        read_pdfs(self, star, pdfs_dir)
        
        Parameters:
            star: integer/string - star identification 
            pdfs_dir: string - PARAM main directory
        
        Returns:
            self.inp: PARAM input dataframe
            self.me: PARAM median output dataframe
            self.mo: PARAM mode output dataframe
            
        Methods:
            getPDFage(self)                   
            self.getPDFmass(self)
            self.getPDFrad(self)
            self.getPDFlogg(self)
            self.getPDFmu0(self)
            self.getPDFdist(self)
            self.getPDFpar(self)
            self.getPDFdist(self)
            self.getPDFAv(self)
            self.getPDFmagobs(self)
            self.getPDFmag(self)
            self.getPDFlogL(self)
            sefl.getPDFs(self)
            getPDFlogTeff(self)
            getPDFmh(self)
            getPDFDnu(self)
    '''
    def __init__(self, star, pdfs_dir):
        global pdf_files, pdf_file_lum, pdf_file_logTeff, pdf_file_mh, pdf_file_dnu
        #global parameters
        self.star = str(star)
        self.dir = pdfs_dir
        
        # Reading PARAM output modes and median:
        fn_inp = glob.glob(pdfs_dir+'*param.in')
        if (len(fn_inp) != 1):
            print ("ERROR: Check if PARAM input file exists.")
            return None
        fn_mo = glob.glob(pdfs_dir+'*param_mo.out')
        if (len(fn_mo) != 1):
            print ("ERROR: Check if PARAM mode output file exists.")
            return None
        fn_me = glob.glob(pdfs_dir+'*param_me.out')
        if (len(fn_me) != 1):
            print ("ERROR: Check if PARAM mode output file exists.")
            return None
        inp = pandas.read_csv(fn_inp[0], sep='\s+')
        inp.columns = inp.columns.str.replace('#', '')
        # Finding NaN values (<-90.0)
        inp[inp.select_dtypes(include='number') < -90.0] = pandas.np.nan
        self.inp = inp[inp.iloc[:,0]==star]
        mo = pandas.read_csv(fn_mo[0], sep='\s+')
        mo.columns = mo.columns.str.replace('#', '')
        # Finding NaN values (<-90.0)
        mo[mo.select_dtypes(include='number') < -90.0] = pandas.np.nan
        self.mo = mo[mo.iloc[:,0]==star]
        me = pandas.read_csv(fn_me[0], sep='\s+')
        me.columns = me.columns.str.replace('#', '')
        # Finding NaN values (<-90.0)
        me[me.select_dtypes(include='number') < -90.0] = pandas.np.nan
        self.me = me[me.iloc[:,0]==star]
        
        # Finding NaN values (<-90.0)
        #for sel in (self.inp, self.mo, self.me):
        #    sel[sel.select_dtypes(include='number') < -90.0] = pandas.np.nan
        

    def getPDFage(self):
        '''
            Reads and returns age PDF.
            Returns:
                self.age: age PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[0]+self.star)==True:
            self.age = pandas.read_csv(self.dir+pdf_files[0]+self.star, sep='\s+')
            self.age.columns = self.age.columns.str.replace('#|\([^)]*\)',"")
        else:
            return None

    def getPDFmass(self):
        '''
            Reads and returns mass PDF.
            Returns:
                self.mass: mass PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[1]+self.star)==True:
            self.mass = pandas.read_csv(self.dir+pdf_files[1]+self.star, sep='\s+')
            self.mass.columns = self.mass.columns.str.replace('#|\([^)]*\)',"")
        else:
            return None
        
    def getPDFrad(self):
        '''
            Reads and returns radius PDF.
            Returns:
                self.rad: radius PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[2]+self.star)==True:
            self.rad = pandas.read_csv(self.dir+pdf_files[2]+self.star, sep='\s+')
            self.rad.columns = self.rad.columns.str.replace('#|\([^)]*\)',"")    
        else:
            return None
        
    def getPDFlogg(self):
        '''
            Reads and returns logg PDF.
            Returns:
                self.logg: logg PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[3]+self.star)==True:
            self.logg = pandas.read_csv(self.dir+pdf_files[3]+self.star, sep='\s+')
            self.logg.columns = self.logg.columns.str.replace('#|\([^)]*\)',"")
        else:
            return None   
    
    def getPDFmu0(self):
        '''
            Reads and returns mu0 PDF.
            Returns:
                self.mu0: mu0 PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[4]+self.star)==True:
            self.mu0 = pandas.read_csv(self.dir+pdf_files[4]+self.star, sep='\s+')
            self.mu0.columns = self.mu0.columns.str.replace('#|\([^)]*\)',"")   
        else:
            return None 
        
    def getPDFdist(self): 
        '''
            Reads and returns distance PDF.
            Returns:
                self.dist: dist PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[5]+self.star)==True:
            self.dist = pandas.read_csv(self.dir+pdf_files[5]+self.star, sep='\s+')
            self.dist.columns = self.dist.columns.str.replace('#|\([^)]*\)',"")
        else:
            return None

    def getPDFpar(self):
        '''
            Reads and returns parallax PDF.
            Returns:
                self.par: parallax PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[6]+self.star)==True:
            self.par = pandas.read_csv(self.dir+pdf_files[6]+self.star, sep='\s+')
            self.par.columns = self.par.columns.str.replace('#|\([^)]*\)',"")
        else:
            return None
            
    def getPDFAv(self):
        '''
            Reads and returns extinction PDF.
            Returns:
                self.Av: extinction PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_files[7]+self.star)==True:
            self.Av = pandas.read_csv(self.dir+pdf_files[7]+self.star, sep='\s+')
            self.Av.columns = self.Av.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None
    
    def getPDFmagobs(self):
        '''
            Reads and returns apparent magnitude PDFs.
            Returns:
                self.magobs: apparent magnitude PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_mag_files[0]+self.star)==True:
            self.magobs = pandas.read_csv(self.dir+pdf_mag_files[0]+self.star, sep='\s+')
            self.magobs.columns = self.magobs.columns.str.replace('#|\([^)]*\)',"") 
        elif os.path.exists(self.dir+pdf_mag_files[1]+self.star)==True:
            self.magobs = pandas.read_csv(self.dir+pdf_mag_files[1]+self.star, sep='\s+')
            self.magobs.columns = self.magobs.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None
        
    def getPDFmag(self):
        '''
            Reads and returns absolute magnitude PDFs.
            Returns:
                self.mag: absolute magnitude PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_mag_files[2]+self.star)==True:
            self.mag = pandas.read_csv(self.dir+pdf_mag_files[2]+self.star, sep='\s+')
            self.mag.columns = self.mag.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None 
    
    def getPDFlogL(self):
        '''
            Reads and returns log luminosity PDFs.
            Returns:
                self.logL: log luminosity PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_file_lum[0]+self.star)==True:
            self.logL = pandas.read_csv(self.dir+pdf_file_lum[0]+self.star, sep='\s+')
            self.logL.columns = self.logL.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None   
    
    def getPDFlogTeff(self):
        '''
            Reads and returns log Teff PDFs.
            Returns:
                self.logTeff: log Teff PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_file_logTeff[0]+self.star)==True:
            self.logTeff = pandas.read_csv(self.dir+pdf_file_logTeff[0]+self.star, sep='\s+')
            self.logTeff.columns = self.logTeff.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None
    
    def getPDFmh(self):
        '''
            Reads and returns metallicity PDFs.
            Returns:
                self.mh: metallicity PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_file_mh[0]+self.star)==True:
            self.mh = pandas.read_csv(self.dir+pdf_file_mh[0]+self.star, sep='\s+')
            self.mh.columns = self.mh.columns.str.replace('#|\([^)]*\)',"")
            self.mh.columns = self.mh.columns.str.replace('\[M/H\]',"mh")
        else:
            return None        
            
    def getPDFDnu(self):
        '''
            Reads and returns Delta nu PDFs.
            Returns:
                self.Dnu: Delta nu PDF dataframe
        '''
        if os.path.exists(self.dir+pdf_file_dnu[0]+self.star)==True:
            self.Dnu = pandas.read_csv(self.dir+pdf_file_dnu[0]+self.star, sep='\s+')
            self.Dnu.columns = self.Dnu.columns.str.replace('#|\([^)]*\)',"") 
        else:
            return None   
        
    def getPDFs(self):
        '''
            Reads and returns age, mass, radius, logg,
            mu0, distance, parallax, extinction, apparent
            and absolute magnitude, and log luminositu PDFs.
            Returns:
                self.age: age PDF dataframe
                self.mass: mass PDF dataframe
                self.rad: radius PDF dataframe
                self.logg: logg PDF dataframe
                self.mu0: mu0 PDF dataframe
                self.dist: distance PDF dataframe
                self.Av: extinction PDF dataframe
                self.magobs: apparent magnitude PDF dataframe
                self.mag: absolute magnitude PDF dataframe
                self.logL: logL luminosity PDF dataframe
        '''
        self.getPDFage()
        self.getPDFmass()
        self.getPDFrad()
        self.getPDFlogg()
        self.getPDFmu0()
        self.getPDFdist()
        self.getPDFpar()
        self.getPDFdist()
        self.getPDFAv()
        self.getPDFmagobs()
        self.getPDFmag()
        self.getPDFlogL()
        
    def label(self, label):
        '''
            Returns a label to the object.
            Returns:
                self.label: string
        '''
        self.label = label


# In[11]:


def read_alambav(obj):
    '''
    Reads alamb_av files and returns a Pandas DataFrame:
    
    df = read_alambav(obj)
    
    Parameters:
        obj: read_param_inp_outs class objects
             or a sting -  directory name
     Returns:
         dataframe
    '''

    if isinstance(obj, read_param_inp_outs):
        if not os.path.isdir(obj.dir+"/pdfs/AlambAv/"):
            print("Error: "+obj.dir+"/pdfs/AlambAv/ does not exist.")
            return 0
        else:
            temp = (pandas.read_csv(obj.dir+"/pdfs/AlambAv/alamb_av_"+str(st), index_col=None, sep="\s+")                    for st in obj.df[obj.df.columns[0]])
            alamb_av  = pandas.concat(temp, axis=0, ignore_index=True)
            alamb_av.columns = alamb_av.columns.str.replace('#', '')   
            return alamb_av
    elif isinstance(obj, str):
         if not os.path.isdir(obj+"/pdfs/AlambAv/"):
            print("Error: "+obj+"/pdfs/AlambAv/does not exist.")
            return 0
         else:
            files = glob.glob(os.path.join(obj+"/pdfs/AlambAv/", "alamb_av*"))     # advisable to use os.path.join as this makes concatenation OS independent

            temp = (pandas.read_csv(f, index_col=None, sep="\s+") for f in files)
            alamb_av  = pandas.concat(temp, axis=0, ignore_index=True) 
            alamb_av.columns = alamb_av.columns.str.replace('#', '')                    
            return alamb_av
    else:
        print("Error: argument is not a string or PARAM objects.")
        return 0


# In[2]:


def rel_diff(x, sx, y, sy):
    '''
        Computes relative difference: (y - x)/x
        and its error: sqrt((sy/x)^2 + ((sx*y)/x^2)^2)
        
        rdiff, erdiff = rel_diff(x, sx, y, sy)
        
        Paramenters:
            x, y, sx, xy: list, tupples or pandas series of numbers
        Returns:
            relative difference and its error
    '''
    return (y - x)/x, pow(pow(sy/x, 2.0) + pow((sx*y)/pow(x, 2.0), 2.0), 0.5)

def abs_diff(x, sx, y, sy):
                          
    '''
        Computes absolute difference: (y - x) and
        its error: sqrt(sx^2 + sy^2)
        
        abdif, eabdiff = abs_diff(x, sx, y, sy)
        
        Paramenters:
            x, y, sx, xy: list, tupples or pandas series of numbers
        Returns:
            absolute difference and its error
    '''
    return (y - x), pow(pow(sy, 2.0) + pow(sx, 2.0), 0.5)


# In[13]:


def differences (obj_x, obj_y, label, **kwargs):
    
    '''
        Computes the mode relative (y-x)/x and absolute differences (y-x)
        for all global parameters between 2 dataframes.
        
        obj_x = differences (obj_x, obj_y, label)
          
        Parameters:
            obj_x, obj_y: read_param_inp_outs objects
            label: string - label for differences
        **kwargs:
            median: default False - if True, then compute differences between median
            silent: default False - if True, then do not print warnings
        
    '''
    global parameters
            
    # Default:
    param = [s + '_mo' for s in parameters] 
    uparam = [s + '_68U_mo' for s in parameters]
    lparam = [s + '_68L_mo' for s in parameters]
    median=False
    silent=False
    
    # kwargs    
    for key, value in kwargs.items():
        if key == 'median':
            median = value
        if key == 'silent':
            silent = value   
    if (median==True):
        param = [s + '_me' for s in parameters]
        uparam = [s + '_68U_me' for s in parameters]
        lparam = [s + '_68L_me' for s in parameters]    
    if (silent==False):
        print ("Computing relative differences: (y-x)/x, and absolute differences: y-x")
            
    if (param[0] not in obj_x.df.columns):
        print ("ERROR: Check the column names of both dataframes")
        return obj_x
    if (param[0] not in obj_y.df.columns):
        print ("ERROR: Check the column names of both dataframes")
        return obj_x
    
    for j in range(0,8): # loop in parameters
        if (j in range(0,3)) or (j in range(5,6)):
            label_diff = 'diffr_'+param[j]+"_"+label
            label_diff_err = 'diffr_'+param[j]+"_"+label+"_err"
            if (label_diff in obj_x.df.columns):
                if (silent == False):
                    print ("WARNING: The series " + label_diff + " already exists.")
            else:
                obj_x.df[label_diff],                 obj_x.df[label_diff_err] = rel_diff(obj_x.df[param[j]],
                                                    abs_unc(obj_x.df[lparam[j]], obj_x.df[uparam[j]]), 
                                                    obj_y.df[param[j]],
                                                    abs_unc(obj_y.df[lparam[j]], obj_y.df[uparam[j]]))       
        else:
            if (param[j] not in obj_x.df.columns) or (param[j] not in obj_y.df.columns):
                continue
            label_diff = 'diffa_'+param[j]+"_"+label
            label_diff_err = 'diffa_'+param[j]+"_"+label+"_err"
            if (label_diff in obj_x.df.columns):
                if (silent == False):
                    print ("WARNING: The series " + label_diff + " already exists.")
            else:
                obj_x.df[label_diff],                 obj_x.df[label_diff_err] = abs_diff(obj_x.df[param[j]],
                                                    abs_unc(obj_x.df[lparam[j]], obj_x.df[uparam[j]]),
                                                    obj_y.df[param[j]],
                                                    abs_unc(obj_y.df[lparam[j]], obj_y.df[uparam[j]])) 
    
    return obj_x
    


# In[14]:


def plot_diffs(obj_x, obj_y, label, *args, **kwargs):
    '''
        Plots relative and absolute difference of age, mass, radius,
        logg, mu0, distance, parallax, and extinction for at least 2 cases.

        plot_diffs(obj_x, obj_y, label, *args, **kwargs)

        Parameters:
            obj_x, obj_y: read_param_inp_outs objects
            label: string - label for differences between case x and y
        *args:
            objs: read_param_inp_outs objects
            labels: string - label for differences between case x and n
        **kwargs:
            median: bolean, default False - if True, then plot differences between median
            diff: bolean, default True - if False, then compute differences
            silent: bolean, default False - if True, then do not print warnings
            title = string - figure title
            savefig = string - file path to save the figure
            error: default True - if False, then do not plot error bars
    
    '''
    global parameters
    
    objs = [obj_x, obj_y]
    dfs = [obj_x.df, obj_y.df]
    labels = [label]
    colors = ('C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7')
    
    xlabel = [u"age (Gyr)", u"$M\ (M_\odot$)", u"$R\ (R_\odot)$", u"log g",
              u"$\mu_0$ (mag)", u"dist (pc)", u"parallax (mas)", u"$A_V$ (mag)"]    
    ylabel = [u"Rel. Diff.", u"Rel. Diff.", u"Rel. Diff.", u"Diff.",
              u"Diff.", u"Rel. Diff.", u"Diff.", u"Diff."] 
    
    # Default
    param = [s + '_mo' for s in parameters]
    uparam = [s + '_68U_mo' for s in parameters]
    lparam = [s + '_68L_mo' for s in parameters]
    median = False
    diff = True
    error = True
    
    # args: adding more objects/dataframes
    for arg in args:
        if isinstance(arg, read_param_inp_outs):
            objs = objs + [arg]
            dfs = dfs + [arg.df]
        if isinstance(arg, basestring):
            labels = labels + [arg]
            
    # kwargs
    for key, value in kwargs.items():
        if key == 'median':
            median = value
        if key == 'diff':
            diff = value  
        if key == 'error':
            error = value   
    if (median==True): # Adopt median values?
        param = [s + '_me' for s in parameters]  
        uparam = [s + '_68U_me' for s in parameters]
        lparam = [s + '_68L_me' for s in parameters]
    if (diff == False): # Need to compute differences?
        obj_x = differences(obj_x, obj_y, label, **kwargs)
        if (len(objs) > 2):
            for i in range(2, len(objs)):
                obj_x = differences(obj_x, objs[i], labels[i-1], **kwargs)

    # ploting
    f, axis = matplotlib.pyplot.subplots(2, 4, figsize=(16, 8))
    f.subplots_adjust(hspace=0.4, wspace=0.4)
    
    for i in range(0,len(dfs)-1):
        for ax, j in zip(axis.flat, range(0,8)): 
            if (param[j] not in dfs[0].columns) or (param[j] not in dfs[i+1].columns):
                continue 
            if (j in range(0,3)) or (j in range(5,6)):    
                label_diff = 'diffr_'+param[j]+"_"+labels[i]
                label_diff_err = 'diffr_'+param[j]+"_"+label+"_err"
            else:
                label_diff = 'diffa_'+param[j]+"_"+labels[i]
                label_diff_err = 'diffa_'+param[j]+"_"+labels[i]+"_err"
            
            if (error==True):
                ax.errorbar(dfs[0][param[j]], dfs[0][label_diff],
                            xerr=abs_unc (dfs[0][lparam[j]], dfs[0][uparam[j]]),
                            yerr=dfs[0][label_diff_err], fmt='o',
                            color=colors[i], label=labels[i])
            else:
                ax.errorbar(dfs[0][param[j]], dfs[0][label_diff],
                            fmt='o',
                            color=colors[i], label=labels[i])
            
            
            #ax.scatter(dfs[0][param[j]], dfs[0][label_diff],
            #        color=colors[i], label=labels[i])
            # To build the legend
            if (j==0):
                handles, labels_plotted = ax.get_legend_handles_labels()
                
            ax.plot([-20.0,15000.0], [0.0,0.0], linestyle='--', color='black')
            
            if (round(min(dfs[0][param[j]])*0.85,2)<0.0):
                ax.set_xlim(round(min(dfs[0][param[j]])*1.25,2), round(max(dfs[0][param[j]])*1.15,2))
            else:
                ax.set_xlim(round(min(dfs[0][param[j]])*0.85,2), round(max(dfs[0][param[j]])*1.15,2))
            
            ax.tick_params(axis='both', direction='in', top=True, right=True)
            ax.set_xlabel(xlabel[j])
            ax.set_ylabel(ylabel[j])
            
    matplotlib.pyplot.tight_layout(pad=4.0, w_pad=1.0)
    if 'title' in kwargs:
        matplotlib.pyplot.suptitle(kwargs['title'], fontsize='xx-large')
        
    # Legend
    f.legend(handles, labels_plotted, bbox_to_anchor=(0.5, 0.07), ncol=len(dfs)-1, fontsize='large') #loc=8

    matplotlib.pyplot.show()  
    if 'savefig' in kwargs:
        f.savefig(kwargs['savefig'], bbox_inches='tight')


# In[15]:


def rel_unc (central, lower, upper):
    '''
        Computes relative uncertainty: (upper-lower)/(2.0*central)
        given an interval
        
        runc = rel_unc (central, lower, upper)
        
        Paramenters:
            central, lower, upper: list, tupples or pandas series of numbers
            
        Returns:
            relative uncertainty 
        
    '''
    return (upper - lower)/(2.0*central)

def abs_unc (lower, upper):
    '''
        Computes absolute uncertainty: (upper-lower)/2.0
        given an interval
        
        aunc = abs_unc (lower, upper)
        
        Paramenters:
            lower, upper: list, tupples or pandas series of numbers
            
        Returns:
            absolute uncertainty 
    '''
    return (upper - lower)/2.0

def frac_unc (central, unc):
    '''
        Computes fractional uncertainty: unc/central
        
        func = frac_unc (central, unc)
        
        Paramenters:
            central, unc: list, tupples or pandas series of numbers
            
        Returns:
            fractional uncertainty
    '''
    return unc/central


# In[16]:


def uncertainties (obj_x, *args):
    
    '''
        Computes the mode relative (y-x)/x and absolute uncertainties (y-x)
        for all parameters between 2 dataframes.
        
        obj_x = uncertainties (obj_x, *args)
          
        Parameters:
            obj_x: read_param_inp_outs class object
        *args:
            median: to compute uncertainties between median values
        Returns:
            original dataframe with new columns with uncertainties
    '''
    global parameters
    print ("Computing relative uncertainties: (X_68_UCI-X_68_LCI)/X, and absolute uncertainties: X_68_UCI-X_68_LCI")
        
    param = [s + '_mo' for s in parameters]
    uparam = [s + '_68U_mo' for s in parameters]
    lparam = [s + '_68L_mo' for s in parameters]
    # median?
    for arg in args:
        if arg == 'median':
            param = [s + '_me' for s in parameters]
            uparam = [s + '_68U_me' for s in parameters]
            lparam = [s + '_68L_me' for s in parameters]
            
    if (param[0] not in obj_x.df.columns):
        print ("ERROR: Check the column names of both dataframes")
        return obj_x

    for j in range(0,8): # loop in parameters
        if (j in range(0,3)) or (j in range(5,6)):
            label_unc = 'rel_unc_'+param[j]
            if (label_unc in obj_x.df.columns):
                print ("WARNING: The series " + label_unc + " already exists.")
            else:
                obj_x.df[label_unc] = rel_unc(obj_x.df[param[j]], obj_x.df[lparam[j]], obj_x.df[uparam[j]])                    
        else:
            if (param[j] not in obj_x.df.columns):
                continue
            label_unc = 'abs_unc_'+param[j]
            if (label_unc in obj_x.df.columns):
                print ("WARNING: The series " + label_unc + " already exists.")
            else:
                obj_x.df[label_unc] = abs_unc(obj_x.df[lparam[j]], obj_x.df[uparam[j]])
    
    return obj_x
    


# In[17]:


def pdf_fill_between(axis, x, y, x1, x2, **kwargs):
    

    x_arr = x.values
    y_arr = y.values
    x_between = []
    y_between = []
    for i in range(0, len(x_arr)):
        if (round(x_arr[i], 4)>=x1.values and round(x_arr[i], 4)<=x2.values):
            x_between = x_between + [x_arr[i]]
            y_between = y_between + [y_arr[i]]
    
    axis.fill_between(x_between, numpy.array(y_between)*0.0, y_between, **kwargs)
  


# In[18]:


def read_tracks(filename):
    '''
    Reads tracks from TRILEGAL and returns a dataframe
    with Mini, Zini, and n as indexes.
    
    read_tracks(filename)
    
    Parameters:
            filename: track file name
    '''
    
    df_trac_header = pandas.read_csv(filename, sep='\s+', nrows=0, index_col=False, skipinitialspace=True )
    trac_header = df_trac_header.columns.tolist()
    trac = pandas.read_csv(filename, sep='\s+', comment='#',
                       index_col=['Mini', 'Zini', 'n'], skipinitialspace=True, names=trac_header[1:])
    return trac


# In[19]:


def read_isocs(filename):
    '''
    Reads isochrones from TRILEGAL and returns a dataframe
    with Zini, Age, and n as indexes.
    
    read_isocs(filename)
    
    Parameters:
            filename: isoc file name
    '''
    
    df_isoc_header = pandas.read_csv(filename, sep='\s+', nrows=0, index_col=False, skipinitialspace=True )
    isoc_header = df_isoc_header.columns.tolist()
    isoc = pandas.read_csv(filename, sep='\s+', comment='#',
                       skipinitialspace=True, names=isoc_header[1:])
    isoc.Zini = isoc.Zini.round(6)
    isoc.Age = isoc.Age.round(0)
    isoc.set_index(['Zini', 'Age', 'n'], inplace=True)
    return isoc


# In[ ]:


def read_kurucz (mh, teff, logg, **kwargs):
    '''
    Reads Kurucz odfnew files closest to the input values:
    metallicity, Teff, and logg and returns a dataframe with
    flux and lambda*flux.
    
    kur, spteff, splogg, kmet = read_kurucz (mh, teff, logg, **kwargs)
    
    Parameters:
        mh: float - metallicity
        teff: float - effective temperature (K)
        logg: float - logaritim of surface gravity
    **kwargs:
        kuruczdir = string - Kurucz odfnew directory path
    
    Returns:
        kur: pandas dataframe
        spteff: float - effective temperature from Kurucz spectrum
        splogg: float - logarithm of surface gravity from Kurucz spectrum
        kmet: float - metallicity from Kurucz spectrum
    '''
    
    # Reading kurucz directory
    if 'kuruczdir' in kwargs:
        kuruczdir = kwargs['kuruczdir']
    else:
        kuruczdir = 'odfnew/'
    if os.path.exists(kuruczdir)==False:
        print("Directory "+kuruczdir+" does not exist.")
        return None
    # Find the closest mh
    kuruczmet = ('m25', 'm20', 'm15', 'm10', 'm05', 'p00', 'p05')
    for kumet in kuruczmet:    
        kuruczfile = kuruczdir+"f"+kumet+"k2odfnew.ext"
        kmet=kumet.replace('m', '-', 1)
        kmet=float(kmet.replace('p', '+', 1))
        kmet=kmet/10.0
        #print(kmet)
        if (kmet+0.25>mh):
            break

    #print("[M/H]=", mh, " will use ", kuruczfile, "\n")
    
    # Find the closest teff and logg
    stopline=235
    lamb_kur=[]
    fo_kur=[] 
    with openfile(kuruczfile) as KUR:
        for il, line in enumerate(KUR):
            key1, value1, key2, value2, temp = line.split(None, 4)
            if (il>=0 and il<stopline):
                lamb_kur.extend(line.split())
            if (key1 == "TEFF"):
                #print(line)
                spteff=float(value1)
                splogg=float(value2)
                if (spteff+125.0>teff and splogg>logg):
                    #print(il, line)
                    break
        for l, line in enumerate(KUR):
            #print(l, line)
            if (l>=0 and l<stopline):                
                fo_kur.extend(line.split())
            if (l>=stopline):
                #print(l, line)
                break


    #print("Teff= %.1f, logg=%.2f, will use spectrum Teff=%.1f logg=%.2f on lines %d--%d" % \
    #      (teff, logg, spteff, splogg, il, stopline+il));

    kur = pandas.DataFrame({'lamb':lamb_kur,'fo':fo_kur})
    kur.lamb = 10.0*kur.lamb.astype(float)
    kur.fo = kur.fo.astype(float)
    kur['loglambda']=numpy.log10(kur.lamb)
    kur['flambda'] = kur.lamb * 4.0 * 3.141596 * 1.0e8 * kur.fo * (2.997950e10/(kur.lamb*kur.lamb))
    kur.loc[kur.flambda<=0.0, 'log_flambda'] = numpy.log10(1.0e-30)
    kur.loc[kur.flambda>0.0, 'log_flambda'] = numpy.log10(kur.loc[kur.flambda>0.0,'flambda'])

    return kur, spteff, splogg, kmet  

# To open file to read:
def openfile(fn):
    try:
        ofile = open(fn, "r")
        return ofile
    except IOError:
        print("Error: File does not exist.")
        return 0


# In[ ]:


# To compute flux
def compute_flambda (lamb, dlamb, mag_ab, emag_ab, rad, erad):
    '''
    Computes log(lamb*F_lamb/erg s-1 cm-2) and log(lamb/Angstrom),
    and retuns log(lamb), slog(lamb)_pos, slog(lamb)_neg,
    log(lamb*F_lamb) and slog(lamb*F_lamb)
    
    loglambda, sloglambdamax, sloglambdamin, \
    log_flambda_abs, sflambda = \
    compute_flambda (lamb, dlamb, mag_ab, emag_ab, rad)
    
    Parameters:
        lamb, dlamb: central band wavelenght and its width in Angstrom
        mag_ab, emag_ab: magnitude and its error in AB system
        rad: radius in solar radius
        
    Returns:
        loglambda, sloglambdamax, sloglambdamin: logarithm
        of waveleght (in Angstrom) and its min and maximum width
        log_flambda_abs, sflambda: logarithm of lamb*F_lamb (in erg s-1 cm-2)
        and its error
    '''
    
    #flux_nu = 10^(-0.4*(m_ab + 48.60))
    fnu_abs_atearth = pow(10.0,(-0.4*(mag_ab+48.60))) # in erg s-1 cm-2 Hz-1
    sfnu_abs_atearth = 0.4*numpy.log(10.0)*fnu_abs_atearth*emag_ab
    
    # R_sun = 6.955e10 cm; 1 pc = 3.085e18
    fnu_abs_atsurface = fnu_abs_atearth / pow((rad*6.955e10)/(10.0*3.085678e18), 2.0) # in erg s-1 cm-2 Hz-1
    sfnu_abs_atsurface = numpy.sqrt( pow((fnu_abs_atsurface*sfnu_abs_atearth)/fnu_abs_atearth ,2.0) +                                 pow( (2.0*fnu_abs_atsurface*erad)/rad, 2.0) )
    # lambda in cm
    lambdac_incm = lamb/1.0e8 # AA to cm
    
    # F_lambda in erg s-1 cm-2
    flambda_abs = lamb * (fnu_abs_atsurface * 2.997950e10)/(lambdac_incm * lambdac_incm) 
    sflambda_abs = (sfnu_abs_atsurface*flambda_abs)/fnu_abs_atsurface
    
    # log_lambda, min e max:
    loglambda = numpy.log10(lamb)
    loglambdamax = numpy.log10(lamb+dlamb/2)
    sloglambdamax = loglambdamax - loglambda 
    loglambdamin = numpy.log10(lamb-dlamb/2)
    sloglambdamin = loglambda - loglambdamin
    
    # log(F_lambda)
    log_flambda_abs = numpy.log10(flambda_abs/1.0e8) ## AAAAAA 1e8 arbitrary factor
    slog_flambda_abs = sflambda_abs /(numpy.log(10.0)*flambda_abs)
    sflambda = 0.4 * emag_ab
    
    return loglambda, sloglambdamax, sloglambdamin, log_flambda_abs, slog_flambda_abs #sflambda
    


# In[ ]:


def compute_flux_and_mag(obj, tab_mag, **kwargs):
    '''
    Computes fluxes and absolute magnitude differences for each filter
    and each star.
    
    cat_mag = compute_flux_and_mag(obj, tab_mag, **kwargs)
    
    Parameters:
        obj: read_param_inp_outs objects
        tab_mag: pandas dataframe with information about tab_mag used
                 (there is an example below)
    
    **kwargs:
        alambda_av = pandas dataframe returned from read_alambav function
                     in the case each star has values of alambda_av
                     otherwise it should be part of tab_mag
        **keywords: options to pass to get_magnitude_names
        
    Returns:
        a dataframe with fluxes and diff_mag for each filter
        
    Example of tab_mag:
    tab_mag = pandas.DataFrame( {'fil' : ('Kep', 'g', 'r', 'i', 'z', 'D51',
                                'J', 'H', 'Ks', 'W1', 'W2', 'W3', 'W4'),
                         'zpkind' : ('AB', 'AB', 'AB', 'AB', 'AB', 'AB',
                                   'Vega', 'Vega', 'Vega', 'Vega', 'Vega', 'Vega', 'Vega'),
                         'zpvega' : (20.680, 19.816, 20.657, 21.289, 21.837, 20.055,
                                   22.932, 23.998, 25.068, 26.864, 28.189, 32.110, 34.907),
                         'zpab' : (20.559, 19.923, 20.515, 20.934, 21.319, 20.118,
                                 22.042, 22.634, 23.234, 24.198, 24.883, 26.970, 28.292),
                         'lambdac' : (6389.68, 4769.82, 6179.99, 7485.86, 8933.86, 5143.24,
                                    12329.79, 16395.59, 21522.05, 33159.26, 45611.97, 107878.20, 219085.73), # in AA
                         'deltalambda' : (3200.0, 1020.0, 920.0, 1040.0, 1340.0, 140.0,
                                        1850.0, 2125.0, 2400.0, 7550.0, 8600.0, 63614.0,42149.0), # in AA
                         'alambda_av' : (0.85946, 1.20585, 0.87122, 0.68319, 0.49246, 1.09497,
                                       0.29434, 0.18128, 0.11838, 0.07134, 0.05511, 0.00220, 0.00000)})
    '''
    
    cat_mag = pandas.DataFrame()
    cat_mag['ID'] = obj.df[obj.df.columns[0]]
    cat_mag['nfil'] = obj.df.nfil_mo
    cat_mag['fils'] = obj.df.fils_mo

    # Names of absolute magnitude columns in the PARAM output file
    absmag_names = labsmag_names = uabsmag_names = []
    if 'logL_mo' in obj.df.columns:
        ran = range(obj.df.columns.get_loc('mbol_95U_mo')+1, obj.df.columns.get_loc('logL_mo'), 5)
    elif 'Av2d_mo' in obj.df.columns:
        ran = range(obj.df.columns.get_loc('mbol_95U_mo')+1, obj.df.columns.get_loc('Av2d_mo'), 5)
    else:
        ran = range(obj.df.columns.get_loc('mbol_95U_mo')+1, obj.df.columns.get_loc('Av_mo'), 5)
    for st in ran:
        absmag_names = absmag_names + [obj.df.columns[st]]
        labsmag_names = labsmag_names + [obj.df.columns[st+1]]
        uabsmag_names = uabsmag_names + [obj.df.columns[st+2]]

    # Names of apparent magnitude columns in the PARAM input file
    appmag_names, sappmag_names = get_magnitude_names(obj, **kwargs)

    for (ind, tmag), absmag, appmag, in zip(tab_mag.iterrows(), absmag_names, appmag_names):
        # print(ind, tmag.fil, absmag, appmag)
        
        erad = 0.5*(obj.df.rad_68U_mo-obj.df.rad_68L_mo)
        # absolute magnitudes in AB system:
        mag_ab = obj.df[absmag]
        if (tmag.zpkind=='Vega'):
            mag_ab = mag_ab + (tmag.zpvega-tmag.zpab)

        emag_ab = (obj.df[uabsmag_names[ind]]-obj.df[labsmag_names[ind]])/2.0
        #print(ind, uabsmag_names[ind], labsmag_names[ind])
        # convert absolute mag to flux (put star at 10 pc):
        cat_mag['loglambda_'+tmag.fil],         cat_mag['sloglambdamax_'+tmag.fil],         cat_mag['sloglambdamin_'+tmag.fil],         cat_mag['log_flambda_abs_'+tmag.fil],         cat_mag['sflambda_'+tmag.fil] = compute_flambda (tmag.lambdac, tmag.deltalambda,
                                                         mag_ab, emag_ab, obj.df.rad_mo, erad)
        
        # Computes absolute magnitudes from observed magnitudes + PARAM distances and extinctions
        # M = m - mu0 - (Av * Alam_Av)
        if ('alambda_av' in kwargs):
            alambda_av = kwargs['alambda_av']
            if isinstance(alambda_av, pandas.DataFrame):
                mag_ab_fap = obj.df[appmag] - obj.df.mu0_mo - obj.df.Av_mo*alambda_av[alambda_av.columns[ind+1]]
        else:
            mag_ab_fap = obj.df[appmag] - obj.df.mu0_mo - obj.df.Av_mo*tmag.alambda_av

        # absolute magnitudes in AB system:
        if (tmag.zpkind=='Vega'):
            mag_ab_fap += (tmag.zpvega-tmag.zpab)

        # Propagating errors:
        if ('alambda_av' in kwargs):
            if isinstance(alambda_av, pandas.DataFrame):
                emag_ab_fap = numpy.sqrt( pow(obj.df[sappmag_names[ind]], 2.0) +                           pow(0.5*(obj.df.mu0_68U_mo-obj.df.mu0_68L_mo), 2.0) +                           pow(alambda_av[alambda_av.columns[ind+1]]*0.5*(obj.df.Av_68U_mo-obj.df.Av_68L_mo), 2.0) )
        else:        
            emag_ab_fap = numpy.sqrt( pow(obj.df[sappmag_names[ind]], 2.0) +                               pow(0.5*(obj.df.mu0_68U_mo-obj.df.mu0_68L_mo), 2.0) +                               pow(tmag.alambda_av*0.5*(obj.df.Av_68U_mo-obj.df.Av_68L_mo), 2.0) ) 
        # convert absolute mag to flux (put star at 10 pc):
        cat_mag['loglambda_'+tmag.fil+'_ap'],         cat_mag['sloglambdamax_'+tmag.fil+'_ap'],         cat_mag['sloglambdamin_'+tmag.fil+'_ap'],         cat_mag['log_flambda_abs_'+tmag.fil+'_ap'],         cat_mag['sflambda_'+tmag.fil+'_ap'] = compute_flambda (tmag.lambdac, tmag.deltalambda,
                                                               mag_ab_fap, emag_ab_fap, obj.df.rad_mo, erad)

        # Difference between mag_ab and mag_ab_fap:
        cat_mag['adiff_mag_'+tmag.fil],         cat_mag['sadiff_mag_'+tmag.fil] = abs_diff(mag_ab, emag_ab, mag_ab_fap, emag_ab_fap)
        
    # Correcting values according to magnitudes used to compute parameters:
    for ind, row in cat_mag.iterrows():
        for f in tab_mag.fil:
            if f not in str(row.fils):
                cols = [s for s in cat_mag.columns if s.endswith((f, f+'_ap'))]
                cat_mag.loc[ind, cols] = numpy.nan
 
    return cat_mag


# In[4]:


def plot_spectrum(star, obj, cat_mag, tab_mag, *args, **kwargs):
    '''
    Plots spectrum and absolute difference in magnitudes (Map-M):
    
    plot_spectrum(star, obj, catmag, tab_mag, *args, **kwargs)
    
    Parameters:
        star: integer/string - number/name of the star (identification)
        obj: read_param_inp_outs objects
        cat_mag: pandas dataframe from compute_flux_and_mag function
        tab_mag: pandas dataframe with information about tab_mag used
                 (there is an example below)    
    *args:
        close: to close figure
    **kwargs:
        title: string - figure title
        savefig: string - file path to save the figure
        **keywords: options to pass to read_kurucz function 
    Returns:
        figure with Kurucz spectrum and magnitude differences
        
    Example of tab_mag:
    tab_mag = pd.DataFrame( {'fil' : ('Kep', 'g', 'r', 'i', 'z', 'D51',
                                'J', 'H', 'Ks', 'W1', 'W2', 'W3', 'W4'),
                         'zpkind' : ('AB', 'AB', 'AB', 'AB', 'AB', 'AB',
                                   'Vega', 'Vega', 'Vega', 'Vega', 'Vega', 'Vega', 'Vega'),
                         'zpvega' : (20.680, 19.816, 20.657, 21.289, 21.837, 20.055,
                                   22.932, 23.998, 25.068, 26.864, 28.189, 32.110, 34.907),
                         'zpab' : (20.559, 19.923, 20.515, 20.934, 21.319, 20.118,
                                 22.042, 22.634, 23.234, 24.198, 24.883, 26.970, 28.292),
                         'lambdac' : (6389.68, 4769.82, 6179.99, 7485.86, 8933.86, 5143.24,
                                    12329.79, 16395.59, 21522.05, 33159.26, 45611.97, 107878.20, 219085.73), # in AA
                         'deltalambda' : (3200.0, 1020.0, 920.0, 1040.0, 1340.0, 140.0,
                                        1850.0, 2125.0, 2400.0, 7550.0, 8600.0, 63614.0,42149.0), # in AA
                         'alambda_av' : (0.85946, 1.20585, 0.87122, 0.68319, 0.49246, 1.09497,
                                       0.29434, 0.18128, 0.11838, 0.07134, 0.05511, 0.00220, 0.00000)})
    '''
 
    df = obj.df.loc[obj.df[obj.df.columns[0]]==star]
    catmag = cat_mag.loc[cat_mag[cat_mag.columns[0]]==star]
    
    # From PARAM input
    dnu = df[df.columns[1]].values
    ednu = df[df.columns[2]].values
    numax = df[df.columns[3]].values
    enumax = df[df.columns[4]].values
    mh = df[df.columns[8]].values
    emh = df[df.columns[9]].values
    teff = df[df.columns[10]].values
    eteff = df[df.columns[11]].values

    # From PARAM output
    logg = df['logg_mo'].values
    elogg = 0.5 * (df['logg_68U_mo'] - df['logg_68L_mo'])
    dist = df['dist_mo'].values
    edist =  0.5 * (df['dist_68U_mo'] - df['dist_68L_mo']) 
    av = df['Av_mo'].values
    eav =  0.5 * (df['Av_68U_mo'] - df['Av_68L_mo']) 

    if (numpy.isnan(mh) or numpy.isnan(teff) or         numpy.isnan(logg) or numpy.isnan(dist) or numpy.isnan(av)):
        print("This star did not converge. Cannot plot spectrum!")  
        return None
    
    # Kurucz file
    kur, spteff, splogg, spmh  = read_kurucz(mh, teff, logg, **kwargs)
    #print(spteff, splogg, spmh )
    # Two subplots sharing x axes
    fig = matplotlib.pyplot.figure( figsize=(14, 10))
    gs = matplotlib.gridspec.GridSpec(2,1, height_ratios=[2,1])

    #AX1
    ax1 = matplotlib.pyplot.subplot(gs[0])
    #plot spectrum
    kur.plot('loglambda', 'log_flambda', ax=ax1, c='black', alpha=0.7, label='', legend=False)
    #print(tab_mag.fil, catmag.fils)
    for f in tab_mag.fil:
        if f not in str(catmag.fils.values):
            continue

        # absolute magnitude   
        #catmag.plot.scatter('loglambda_'+f, 'log_flambda_abs_'+f, ax=ax1,c='C0', s=30)
        xerr = [catmag['sloglambdamin_'+f], 
                catmag['sloglambdamax_'+f]]
        yerr = catmag['sflambda_'+f]

        ax1.errorbar(catmag['loglambda_'+f],
                     catmag['log_flambda_abs_'+f],
                     fmt='none',
                     xerr=xerr,
                     yerr=yerr,
                     alpha=1.0, ecolor = 'C0') #, color= 'red'


        # observed magnitude
        #catmag.plot.scatter('loglambda_'+f+'_ap', 'log_flambda_abs_'+f+'_ap', ax=ax1, c='C1', s=30)

        xerr = [catmag['sloglambdamin_'+f+'_ap'], 
                catmag['sloglambdamax_'+f+'_ap']]
        yerr = catmag['sflambda_'+f+'_ap']

        ax1.errorbar(catmag['loglambda_'+f+'_ap'],
                     catmag['log_flambda_abs_'+f+'_ap'],
                     fmt='none',
                     xerr=xerr,
                     yerr=yerr,
                     alpha=1.0, ecolor = 'C1') #, color= 'red'

    ax1.set_ylim(8.8,11.5)
    ax1.set_ylabel('$\log$ ${(\lambda F_\lambda)}$ (erg s${^-1}$ cm$^{-2}$)')

    #AX2
    ax2 = matplotlib.pyplot.subplot(gs[1])
    for f in tab_mag.fil:
        if f not in str(catmag.fils.values):
            continue

        xerr = [catmag['sloglambdamin_'+f], 
                catmag['sloglambdamax_'+f]]
        yerr = catmag['sadiff_mag_'+f]

        ax2.errorbar(catmag['loglambda_'+f],
                     catmag['adiff_mag_'+f],
                     fmt='none',
                     xerr=xerr,
                     yerr=yerr,
                     alpha=1.0, ecolor = 'C0') 

        ax2.annotate(f, (catmag['loglambda_'+f]+0.01,
                     catmag['adiff_mag_'+f] - \
                         catmag['sadiff_mag_'+f]),
                    fontsize='xx-large')#txt, (z[i], y[i]))

    ax2.plot([0,8], [0,0], '--',  c='black', alpha=0.7)
    ax2.set_ylim([-0.22,0.22])
    ax2.set_ylabel('$\Delta$M$_\lambda$')
    ax2.set_xlabel('$\log$ ${\lambda}$ $(\AA)$')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    fig.subplots_adjust(hspace=0)
    matplotlib.pyplot.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
    
    if 'title' in kwargs:
        tit = kwargs['title']
    else:
        tit = str(star) +         "\n$T_\mathrm{eff}$=("+"{:.0f}$\pm${:.0f}) K".format(float(teff), float(eteff)) +         ", [Fe/H]=({:.2f}$\pm${:.2f}) dex".format(float(mh), float(emh)) +         ", $\\Delta\\nu$=({:.2f}$\pm${:.2f}) $\mu$Hz".format(float(dnu), float(ednu)) +         ", $\\nu_\mathrm{max}$=("+"{:.2f}$\pm${:.2f}) $\mu$Hz,".format(float(numax), float(numax)) +         "\n$\log{g}$=("+"{:.3f}$\pm${:.3f}) dex".format(float(logg), float(elogg)) +         ", d=({:.2f}$\pm${:.2f}) pc".format(float(dist), float(edist)) +         ", $A_V$=({:.2f}$\pm${:.2f}) mag".format(float(av), float(eav))

    ax1.set_title(tit)

    for ax in (ax1, ax2):        
        ax.set_xlim(3.5,4.8)
        
    lab_spec = "Closest spectrum: " +     "$T_\mathrm{eff}$="+"{:.0f} K, ".format(spteff)+"$\log{g}$="+"{:.2f} dex, ".format(splogg) +     "[Fe/H]={:.2f} dex".format(spmh)     
    ax1.annotate(lab_spec, (0.1, 0.05), xycoords='axes fraction', fontsize="xx-large")

    if 'savefig' in kwargs:
        fig.savefig(kwargs['savefig'])#, bbox_inches='tight') 
        
    if 'close' in args:
        matplotlib.pyplot.close()
    else:
        matplotlib.pyplot.show()



# :star2: PARAM v1.5.1 Python Library :star2:

PARAM is a Bayesian tool that computes stellar properties by comparing data with values derived from stellar models. The statiscal method and details about the code can be found in [da Silva et al. (2006)](http://adsabs.harvard.edu/abs/2006A%26A...458..609D),  [Rodrigues et al. (2014)](http://adsabs.harvard.edu/abs/2014MNRAS.445.2758R), and [Rodrigues et al. (2017)](http://adsabs.harvard.edu/abs/2017MNRAS.467.1433R).

This repository keeps Python functions used to analyse PARAM output data. You can find a [jupyter notebook here](Examples.ipynb) and [its html version](Examples.html) with examples on how to use _parampy_ functions using real data.

In order to analyse the data provided by PARAM, I developed the following functions:

* [read_param_inp_outs](#read_param_inp_outs)
* [plot_npdfs](#plot_npdfs)
* [get_magnitude_names ](#get_magnitude_names)
* [plot_npdfs_mag](#plot_npdfs_mag)
* [plot_pdfs_m_M_mu_mu0](#plot_pdfs_m_M_mu_mu0)
* [class read_pdfs](#class-read_pdfs)
* [read_alambav](#read_alambav)
* [rel_diff](#rel_diff)
* [abs_diff](#abs_diff)
* [differences](#differences)

## read_param_inp_outs

A class that keeps information about PARAM directory and reads and stores the PARAM input and output files in one pandas dataframe.
```    
read_param_inp_outs(self, pdfs_dir, *args)
```
__Parameters:__    
pdfs_dir: string - PARAM main directory   

__Arguments:__   
    median: to read median output   
    inp_mo_pdf: to read input mode output   
    inp_me_pdf: to read input median output   
    
__Rerturns:__   
     a pandas dataframe with all  concatenated files by identifier   
    
## plot_npdfs
Plots a figure with pdfs of age, mass, radius, logg, mu0, distance, parallax, and extinction for at least two cases with the mode as default central tendency.   
```
plot_pdfs(star, obj_a, label_a, obj_b, label_b, *args, **kwargs)
```

__Parameters:__   
    star: integer - number of the star identification    
    obj_a, obj_b: read_param_inp_outs objects   
    label_a, label_b: string - label for case a and case b   
    
__Arguments:__   
    obj: new read_param_inp_outs object   
    label: label for the new object case    
    median: to plot the median as central tendency (default mode)   

__Keyword arguments:__   
    title = string - figure title   
    savefig = string - file path to save the figure   
    **keywords: options to pass to matplotlib plotting method   
    
## get_magnitude_names   
Gets apparent magnitude names used to run PARAM.   
```
names = get_magnitude_names(obj, **kwargs)
```

__Parameters:__   
    obj: read_param_inp_outs object   
    
__Keyword arguments:__   
    last_mag_col: string - name of the last magnitude column   

__Returns:__   
    a list with names of apparent magnitude columns in the object   

## plot_npdfs_mag
Plots PDFs of apparent and absolute magnitudes for at least two cases.   
```
plot_pdfs_mag(star, df_a, df_b, dir_a, dir_b, label_a, label_b,
              *args, **kwargs)
```

__Parameters:__   
    star: integer - number of the star identification    
    obj_a, obj_b: read_param_inp_outs objects   
    label_a, label_b: string - label for case a and case b   
    
__Arguments:__   
    obj: new read_param_inp_outs object   
    label: label for the new object case   
    median: to take into account the median values   
    
__Keyword arguments:__   
    pdf_files = string or list of strings - PDF filenames   
    title = string - figure title   
    savefig = string - file path to save the figure   
    **keywords: options to pass to matplotlib plotting method or get_magnitude_names

## plot_pdfs_m_M_mu_mu0

Plots PDFs of apparent, absolute magnitudes, apparent distance modulus, and distance modulus for one star.   
```
plot_pdfs_m_M_mu_mu0(star, obj, *args, **kwargs)
```

__Parameters:__   
    star: integer - number of the star identification    
    obj: read_param_inp_outs objects   
    
__Arguments:__   
    median: to take into account the median values   
    
__Keyword arguments:__   
    pdf_files: list of strings with PDF magobs, mag, mu and mu0 filenames   
    fil_names: list of strings with filter names sorted according to PARAM tab_mag file   
    alamb_av: list of floats with alamb_av coefficients sorted according to PARAM tab_mag file   
    title: string - figure title   
    savefig: string - file path to save the figure   
    **keywords: options to pass to matplotlib plotting method or get_magnitude_names   


## class read_pdfs
A class that keeps info about a single star and reads and stores the PDF files in dataframes.
 ```   
read_pdfs(self, star, pdfs_dir)
```

__Parameters:__   
    star: integer - number of the star identification    
    pdfs_dir: string - PARAM main directory   

__Returns:__   
    self.inp: PARAM input dataframe   
    self.me: PARAM median output dataframe   
    self.mo: PARAM mode output dataframe   
       
__Methods:__
    getPDFage(self)                   
    self.getPDFmass(self)   
    self.getPDFrad(self)   
    self.getPDFlogg(self)   
    self.getPDFmu0(self)   
    self.getPDFdist(self)   
    self.getPDFpar(self)   
    self.getPDFdist(self)   
    self.getPDFAv(self)   
    self.getPDFmagobs(self)   
    self.getPDFmag(self)   
    self.getPDFlogL(self)   
    sefl.getPDFs(self)   
    getPDFlogTeff(self)   
    getPDFmh(self)   
    getPDFDnu(self)   

## read_alambav

Reads alamb_av files and returns a Pandas DataFrame.   
```
df = read_alambav(obj)
```

__Parameters:__   
    obj: read_param_inp_outs class objects or a sting with directory name   
         
__Returns:__   
    dataframe   
         
## rel_diff

Computes the relative difference: (y - x)/x and its error: sqrt((sy/x)^2 + ((sx*y)/x^2)^2).   
```
rdiff, erdiff = rel_diff(x, sx, y, sy)
```
   
__Paramenters:__   
    x, y, sx, xy: list, tupples or pandas series of numbers   
    
__Returns:__   
    relative difference and its error   
    
## abs_diff

Computes absolute difference: (y - x) and its error: sqrt(sx^2 + sy^2)   
```
abdif, eabdiff = abs_diff(x, sx, y, sy)
```

__Paramenters:__   
    x, y, sx, xy: list, tupples or pandas series of numbers   
    
__Returns:__   
    absolute difference and its error   
    
## differences

Computes the mode relative (y-x)/x and absolute differences (y-x) for all global parameters between 2 dataframes.   
```
differences (obj_x, obj_y, label)
```

__Parameters:__
    obj_x, obj_y: read_param_inp_outs objects   
    label: string - label for differences   
    
__Keywords arguments:__
    median: boolean, default False - if True, then compute differences between median   
    silent: boolean, default False - if True, then do not print warnings   


:snail: :snail: WORK IN PROGRESS...

